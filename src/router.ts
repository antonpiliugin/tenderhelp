import { createRouter, createWebHistory } from 'vue-router'
import Home from './views/Home.vue'
import LoggedIn from './views/LoggedIn.vue'
import SignUp from './views/SignUp.vue'

const routes = [
    {
        path: '/',
        name: 'Home',
        component: Home
    },
    {
        path: '/logged-in',
        name: 'LoggedIn',
        component: LoggedIn
    },
    {
        path: '/sign-up',
        name: 'Sign-Up',
        component: SignUp
    }
]

const router = createRouter({
    history: createWebHistory(),
    routes
})

export default router